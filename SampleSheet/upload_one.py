import sampleSheetParser
import pandas as pd
import json
import pathlib
import re
import logging
import mimetypes
import requests
import sys

# run with the command pytho3 upload_one.py RUN_ID FilePath

def upload(file_path, data_list,file_name):
    url = 'http://wpsandbox01.predicine.com:5002/api/v1/samplesheet/'
    try:
        file = open(file_path, 'rb')
    except Exception as e:
        print(str(e))

    #summary
    data_summary ={
    'data':json.dumps(data_list),
    'operator':'WaterMelon',
    'orig_file_path':file_path,
    'file_type':mimetypes.guess_type(file_path, strict=True),
    'file_name':str(file_name)+"_sample_sheet.csv"
    }
    file_summary = {'file':file}

    #the request
    r = requests.post(url, data=data_summary, files=file_summary)
    print(r.text)
    file.close()

def main():
    parser = sampleSheetParser.SampleSheet_Parser()
    file_name = str(sys.argv[1])
    file_path = pathlib.Path(sys.argv[2])
    try:
        result = parser.parse(file_path)
        upload(file_path,result,file_name)
    except:
        print("Fail to upload File")


if __name__ == "__main__":
    main()
