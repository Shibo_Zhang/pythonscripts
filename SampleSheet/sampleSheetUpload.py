import sampleSheetParser
import pandas as pd
import json
import pathlib
import re
import logging
import mimetypes
import requests
import smtplib

def upload(file_path, data_list,file_name):
    url = 'http://wpsandbox01.predicine.com:5002/api/v1/samplesheet/'
    try:
        file = open(file_path, 'rb')
    except Exception as e:
        print(str(e))

    #summary
    data_summary ={
    'data':json.dumps(data_list),
    'operator':'WaterMelon',
    'orig_file_path':file_path,
    'file_type':mimetypes.guess_type(file_path, strict=True),
    'file_name':str(file_name)+"_sample_sheet.csv"
    }
    file_summary = {'file':file}

    #the request
    r = requests.post(url, data=data_summary, files=file_summary)
    print(r.text)
    file.close()

def read_json_to_dict(filePath):
    data = {}
    with open(filePath) as json_file:
        data = json.load(json_file)

    return data

def write_dict_to_json(file_dict:dict, outputpath):

    with open(outputpath, "w") as outfile:
        json.dump(file_dict, outfile)

# funcitons to send emamils
def send_email(email_address, content):
    mailserver = smtplib.SMTP('smtp.office365.com',587)
    mailserver.ehlo()
    mailserver.starttls()
    mailserver.login('smtp@predicine.com', 'Gug52641')
    #Adding a newline before the body text fixes the missing message body
    mailserver.sendmail('smtp@predicine.com',email_address,content)
    mailserver.quit()

    logging.info("Email sent")


def main():
    # set email_flag = True if want to enable email senting
    email_flag = False
    # import sample sheet parser class from sampleSheetParser
    parser = sampleSheetParser.SampleSheet_Parser()

    # read files from scanned_info.json
    # change the paths if need to upload other files
    json_path = pathlib.Path.cwd() / "scanned_info.json"
    file_list = read_json_to_dict(json_path)
    
    error_file = {}
    error_file_path = new_file_path = pathlib.Path.cwd() / "error_file.json"
    for file in file_list:
        file_path = pathlib.Path(file_list[file])
        try:
            result = parser.parse(file_path)
            upload(file_path,result,file)
        except Exception as e:
            print(str(e))
            error_file[file] = file_list[file]
    
    write_dict_to_json(error_file,error_file_path)


    if email_flag:
        email_address = 'sbzhang@predicine.com'
        content = '\nUploading complete'
        send_email(email_address,content)

if __name__ == "__main__":
    main()
