import pandas as pd
import json
import pathlib
import re
import logging
import mimetypes
import requests
import csv

logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)s - %(asctime)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


class SampleSheet_Parser:
    # initialize the matching mapping and keyword control mapping
    def __init__(self):
        self.field_mapping = self.field_matching_map()
        self.keyword_mapping = self.keyword_matching_map()
        # add keyword control pattern
        self.PATTERN_V1 = re.compile("PREDICINEMRD_V1_+P[a-zA-Z0-9]+")
        self.PATTERN_V1B = re.compile("PREDICINEMRD_V1B_+P[a-zA-Z0-9]+")

    # create a dictionary map the fields
    def field_matching_map(self):
        # if new entity field needs, just add it to the mapping dictionary
        # concate the the field name and field type into string with format "Field_Name, Type"
        mapping = dict()
        mapping["sample_name"] = "Sample_ID, char"
        mapping["flowcell_lane"] = "Lane, char"
        mapping["i7_name"] = "I7_Index_ID, char"
        mapping["i7_seq"] = "index, char"
        mapping["i5_name"] = "I5_Index_ID, char"
        mapping["i5_seq"] = "index2, char"
        mapping["project"] = "Sample_Project, char"
        mapping["el_panel_name"] = "Panel, char"
        mapping["s_assay_input_material_type"] = "Assay_Input_Material_Type, char"
        mapping["run_id"] = "Run_ID, char"
        mapping["description"] = "Description, char"
        #mapping[""] = "Input_NA_Amount_for_LibraryPrep_ng, float"
        #mapping[""] = "BA_Library_Total_Yield_ug, float"
        #mapping[""] = "control, char"
        mapping["s_specimen_type"] = "specimen_type, char"
        mapping["adapter_type"] = "adaptertype, char"
        mapping["p_analysis_type"] = "analysis_type, char"
        mapping["work_order_info"] = "work_order, char"
        mapping["original_adapter"] = "original_adapter, char"
        mapping["ali_na_dna_amout_ng"] = "Total_DNA_input, float"
        mapping["lb_yield_ug"] = "Library_yield_ug, float"
        mapping["ali_na_cfdna_amount_ng"] = "Total_cfDNA_input, float"
        mapping["ali_lb_amount_ug"] = "Library_DNA_Input_ug, float"
        mapping["el_molarity_nm"] = "Enriched_library_FA_or_BA_nM, float"
        mapping["el_conc_ng_ul"] = "Qubit_ng_ul, float"
        mapping["pipeline_version"] = "pversion, char"
        mapping["s_study_type"] = "Category, char"
        return mapping

    # create a dictionary map the keyword filter matching
    # all key should be transfer to uppercase first
    def keyword_matching_map(self):
        mapping = dict()

        # Category

        # RUO
        mapping["RUO"] = "RUO"
        # CLIA
        mapping["CLIA"] = "CLIA" 
        mapping["CO"] = "CLIA"
        # RnD
        mapping["RND"] = "RnD"
        # Validation
        mapping["VALIDATION"] = "Validation"
        # IVD
        mapping["IVD"] = "IVD"

        # Specimen_type

        # Whole blood 
        mapping["Fresh WB"] = "Whole blood"
        mapping["Fresh_WB"] = "Whole blood"
        mapping["FRESHWB"] = "Whole blood"
        mapping["WHOLE_BLOOD"] = "Whole blood"
        mapping["WHOLEBLOOD"] = "Whole blood"
        # Plasma
        mapping["PLASMA"] = "Plasma"    
        mapping["FROZENPLASMA"] = "Plasma"
        # cfDNA   
        mapping["CFDNA"] = "cfDNA"
        # RNA
        mapping["RNA"] = "RNA"
        # Buffy Coat
        mapping["BUFFY_COAT"] = "Buffy Coat"
        # gDNA
        mapping["GDNA"] = "gDNA"
        # Serum
        mapping["SERUM"] = "Serum"
        # Urine
        mapping["URINE"] = "Urine"
        # Cell pellet
        mapping["CELL PELLET"] = "Cell pellet"
        mapping["CELL_PELLET"] = "Cell pellet"
        # FFPE 
        mapping["FFPE"] = "FFPE"
        mapping["TISSUE"] = "FFPE"
        mapping["FFPE SLIDES"] = "FFPE"
        mapping["FFPE_SLIDES"] = "FFPE"
        mapping["FFPEE BLOCK"] = "FFPE"
        # FFPE DNA
        mapping["FFPE DNA"] = "FFPE DNA"
        # Bone Marrow Aspirate
        mapping["BONE MARROW"] = "Bone Marrow Aspirate"
        mapping["BONE_MARROW"] = "Bone Marrow Aspirate"
        mapping["BONE_MARROW_ASPIRATE"] = "Bone Marrow Aspirate"
        # CSF
        mapping["CEREBRAL_SPINAL_FLUID"] = "CSF"
        mapping["CSF"] = "CSF"
        # Platelet
        mapping["PLATELET"] = "Platelet"
        # Nucleic Acid
        mapping["NUCLEIC_ACID"] = "Nucleic Acid"
        # Cryopreserved cells
        mapping["PBMC_BM"] = "Cryopreserved cells"
        mapping["CRYOPRESERVED CELLS"] = "Cryopreserved cells"
        mapping["PBMC"] = "Cryopreserved cells"
        mapping["CRYO PELLET"] = "Cryopreserved cells"
        mapping["CRYOPRESERVED PBMC"] = "Cryopreserved cells"
        # Cell line
        mapping["CELL LINE"] = "Cell line"
        mapping["CELL_LINE"] = "Cell line"
        mapping["CELLLINE"] = "Cell line"
        mapping["MIXED_CELLLINE"] = "Cell line"

        # Panel
        
        # PREDICINECARE_V2B
        mapping["PREDICINECARE_V2B"] = "PREDICINECARE_V2B"
        
        # PREDICINECARELITE_V1
        mapping["PREDICINECARELITE_V1"] = "PREDICINECARELITE_V1"
        
        # PREDICINEWES_V1
        mapping["PREDICINEWES_V1"] = "PREDICINEWES_V1"
        mapping["WES"] = "PREDICINEWES_V1"
        mapping["IDT_EXOME_V2__PREDICINEATLAS_V2__PREDICINEHRD_V1"] = "PREDICINEWES_V1"
        # PREDICINEATLAS_V1
        mapping["PREDICINEATLAS_V1"] = "PREDICINEATLAS_V1"
        mapping["PREDICINEATLAS"] = "PREDICINEATLAS_V1"
        # PREDICINELDT_V1
        mapping["PREDICINELDT_V1"] = "PREDICINELDT_V1"
        mapping["PREDICINELITE_V1__DDR_V2__DDR_PATCH"] = "PREDICINELDT_V1"
        mapping["PREDICINELDT"] = "PREDICINELDT_V1"
        # PREDICINERESEARCH
        mapping["PREDICINE_RESEARCH"] = "PREDICINERESEARCH"
        mapping["PREDICINERESEARCH"] = "PREDICINERESEARCH"
        # PREDICINEDDR_V2
        mapping["PREDICINEDDR_V2"] = "PREDICINEDDR_V2"
        mapping["DDR_V2"] = "PREDICINEDDR_V2"
        # PredicineLITE_V1        
        mapping["PREDICINELITE_V1"] = "PredicineLITE_V1"
        mapping["PREDICINELITE"] = "PredicineLITE_V1"
        mapping["PREDICINELITE"] = "PREDICINELITE_V1__DDR_V2__DDR_PATCH"
        # PREDICINEPCA_V1
        mapping["PREDICINEPCA_V1"] = "PREDICINEPCA_V1"
        mapping["PREDICINEPCA"] = "PREDICINEPCA_V1"        
        # PREDICINEPCA_V1
        mapping["RNA_FUSION_V1__RNA_HS_V1__RNA_FUS_PATCH"] = "PREDICINERNA_V1"
        mapping["RNA_FUSION_V1__RNA_HS_V1__RNA_FUSION_PATCH"] = "PREDICINERNA_V1"
        mapping["RNA_HS_V1__RNA_FUSION_V1__RNA_FUSION_PATCH__ALK"] = "PREDICINERNA_V1"
        # PREDICINEFUS_V1
        mapping["RNA_Fusion_V1__RNA_FUS_Patch"] = "PREDICINEFUS_V1"
        mapping["RNA_FUSION_V1__RNA_FUSION_PATCH"] = "PREDICINEFUS_V1"
        mapping["PREDICINEFUS"] = "PREDICINEFUS_V1"
        # PAN_V4
        mapping["PAN_V4"] = "PAN_V4"
        mapping["PAN_ALL3"] = "PAN_V4"
        # PAN_V4_CORE__DNA_FUSION
        mapping["PAN_V4_CORE__DNA_FUSION"] = "PAN_V4_CORE__DNA_FUSION"
        mapping["PANCERCANCER_CORE_FUSION"] = "PAN_V4_CORE__DNA_FUSION"
        # ACT_V1
        mapping["ACT_V1"] = "ACT_V1"
        mapping["PREDICINEACT_V1"] = "ACT_V1"

        return mapping
    
    # parse the csv file into json format
    def parse(self, filePath):

        row_list = []
        index = 0
        with open(filePath, 'r') as csv_file:
            reader = csv.reader(csv_file)
            for i, row in enumerate(reader):
                row_list.append(row)
                if len(row) >0 and row[0] == '[Data]':
                    index = i
            csv_file.close()
        df = pd.DataFrame(row_list[index+2:], columns=row_list[index+1])

        logging.info("Read"+ ": " + str(filePath))
        # create a list store all rows
        result_list = []     
        try:
            # store one row in dict format and iterate through all dataframe
            for index in range(df.shape[0]):
                try:
                    data_row = dict()
                    # transfer Field Name to Database Entity Field
                    for key in self.field_mapping:
                        value = self.field_mapping[key].split(", ")
                        field_name = value[0]
                        entity_type = value[1]
                        if field_name in df.columns:
                            if not df.loc[index,field_name]:
                                acutal_value= None
                            else:
                                acutal_value = str(df.loc[index,field_name])
                                if acutal_value.upper() in self.keyword_mapping:
                                    acutal_value = self.keyword_mapping[acutal_value.upper()]

                            if acutal_value != "nan" and acutal_value != None:
                                if entity_type == "int":
                                    data_row[key] = int(acutal_value)
                                elif entity_type == "float":
                                    data_row[key] = float(acutal_value)
                                else:
                                    data_row[key]= acutal_value
                            elif acutal_value == "null":
                                data_row[key] = None   
                            else:
                                data_row[key] = None
                        else:
                            data_row[key] = None
                    
                    # append the data_row to result list
                    result_list.append(data_row)
                except:
                    logging.error("Errors in line:" + str(index))
            
            return(result_list)
        except:
            logging.error("Cannot read file"+ ": " + str(filePath))




def main():
    parser = SampleSheet_Parser()
    file_path = pathlib.Path("sample_sheet.csv")
    result = parser.parse(file_path)
    print(result)

if __name__ == "__main__":
    main()



