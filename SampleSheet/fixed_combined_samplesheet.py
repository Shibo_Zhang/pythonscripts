import sampleSheetParser
import fixed_sampleSheetScanning
import pandas as pd
import json
import pathlib
import os
import re
import logging
import mimetypes
import requests
import smtplib
import os
import shutil

def upload(file_path, data_list,file_name):
    url = 'http://wpsandbox01.predicine.com:5002/api/v1/samplesheet/'
    try:
        file = open(file_path, 'rb')
    except Exception as e:
        print(str(e))

    #summary
    data_summary ={
    'data':json.dumps(data_list),
    'operator':'WaterMelon',
    'orig_file_path':file_path,
    'file_type':mimetypes.guess_type(file_path, strict=True),
    'file_name':str(file_name)+"_sample_sheet.csv"
    }
    file_summary = {'file':file}

    #the request
    r = requests.post(url, data=data_summary, files=file_summary)
    print(r.text)
    file.close()
    return r.text

def read_json_to_dict(filePath):
    data = {}
    with open(filePath) as json_file:
        data = json.load(json_file)

    return data

def write_dict_to_json(file_dict:dict, outputpath):

    with open(outputpath, "w") as outfile:
        json.dump(file_dict, outfile)

def translator(error):
    if error[0] == "{":
        err_list = error.split(', ')
        # eliminate the curly brakets
        err_list[0] = err_list[0][1:]
        err_list[-1] = err_list[-1][:-1]
        # add some translation
        for i in range(len(err_list)):
            row_num = int(err_list[i][0]) + 2
            err_list[i] = "Errors in row: " + str(row_num) + err_list[i][1:]
        result = ""
        for err in err_list:
            result += err + "<br>"
        return result
    else:
        err_list = error.split('; ')
        err_list[-1] = err_list[-1][:-1]
        result = ""
        for err in err_list:
            result += err + "<br>"
        return result

def send_email(Error_msg):
    message = """From: lims.help@predicine.com
To: lims.help@predicine.com, sbzhang@predicine.com
MIME-Version: 1.0
Content-type: text/html
Subject: Fixed SampleSheet Uploading Errors
Auto-Submitted:no
Precedence:first



    
Hi,
<br>
Those sample sheets have error during uploading or parsing after fixing.
<br>

"""
    
    
    for error in Error_msg:
        message += "File Path: " +  str(error) + "<br> Error Detail:  "+ translator(Error_msg[error]) + "<br><br><br>"
    


    try:
        mailserver = smtplib.SMTP('smtp.office365.com',587)
        mailserver.ehlo()
        mailserver.starttls()
        mailserver.login('lims.help@predicine.com', 'Fum05423')
        #Adding a newline before the body text fixes the missing message body
        mailserver.sendmail('lims.help@predicine.com',['lims.help@predicine.com','sbzhang@predicine.com'],message)
        mailserver.quit()

        print("email sent")
    except smtplib.SMTPException as e:
        print(e) 
    
    return 


def main():
    fixed_sampleSheetScanning.main()
    # import sample sheet parser class from sampleSheetParser
    parser = sampleSheetParser.SampleSheet_Parser()

    # read files from scanned_info.json
    # change the paths if need to upload other files
    
    #new_file =  pathlib.Path.cwd() / "new_file.json"
    #scanned_file = pathlib.Path.cwd() / "scanned_info.json"
    new_file = pathlib.Path("/home/sbzhang/scripts/pythonscripts/SampleSheet/fixed_new_file.json")
    scanned_file = pathlib.Path("/home/sbzhang/scripts/pythonscripts/SampleSheet/fixed_scanned_info.json")

    if(os.path.exists(new_file)):
        scanned_file = new_file

    file_list = read_json_to_dict(scanned_file)
    
    error_file = {}
    error_msg = {}
    error_file_path = pathlib.Path("/home/sbzhang/scripts/pythonscripts/SampleSheet/fixed_error_file.json")
    for file in file_list:
        file_path = pathlib.Path(file_list[file])
        try:
            result = parser.parse(file_path)
            
        except:
            error_msg[file_list[file]] = "Error in parsing files"
            error_file[file] = file_list[file]
            continue
        
        response = upload(file_path,result,file)
        temp_dict = json.loads(response)
        if temp_dict["success"] == False:
            if not temp_dict["err_message"][2:12] == "Duplicated":
                error_msg[file_list[file]] = temp_dict["err_message"]
                error_file[file] = file_list[file]

    write_dict_to_json(error_file,error_file_path)
    
    if error_file:
        send_email(error_msg)


if __name__ == "__main__":
    main()
