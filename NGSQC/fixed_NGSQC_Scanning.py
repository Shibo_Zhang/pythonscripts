import os
import json
import csv
import pathlib
import re
import logging
import smtplib





RUN_DIR_REGEX = re.compile("\d+_.+_.+_.+$")
OLD_RUN_DIR_REGEX = re.compile("\d+_[a-zA-Z0-9]\d+_\d+_[a-zA-Z0-9]+_old")
NGSQC_REGEX = re.compile("\d+_[a-zA-Z0-9]\d+_\d+_[a-zA-Z0-9]+_combined_NGSQC_Summary_all.csv")

logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)s - %(asctime)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

# generate the paths of directories to be scanned
# return the list of paths
def generate_paths():
    base_path_ls = []
        
    base_path_ls.append("/prednet/data03/DevHome/kiwi_file_maintenance/files_after_fix")

    return base_path_ls

# write python dictionary into Json file 
def write_dict_to_json(file_dict:dict, outputpath):

    with open(outputpath, "w") as outfile:
        json.dump(file_dict, outfile)

# compared the two dictionary and return the difference
def read_json_to_dict(filePath):
    data = {}
    with open(filePath) as json_file:
        data = json.load(json_file)

    return data

# compared the two dictionary and return the difference
def compare_diff(past_scanned:dict , current_scanned:dict):
    new_dict = {}
    for file in current_scanned:
        if file in past_scanned:
            continue
        else:
            new_dict[file] = current_scanned[file]

    return new_dict

# funcitons to send emamils
def send_email(email_address, content):
    mailserver = smtplib.SMTP('smtp.office365.com',587)
    mailserver.ehlo()
    mailserver.starttls()
    mailserver.login('smtp@predicine.com', 'Gug52641')
    #Adding a newline before the body text fixes the missing message body
    mailserver.sendmail('smtp@predicine.com',email_address,content)
    mailserver.quit()

    logging.info("Email sent")


class RunScanner:

    def __init__(self):
        self.runs = {}
        self.no_access = []
        self.duplicates = []


    def print_stats(self):
        """print some summary stats"""
        logging.info("Number of Runs: " + str(len(self.runs)))
        logging.info("Number of files with No Access: " + str(len(self.no_access)))
        logging.info("Number of duplicate Runs: " + str(len(self.duplicates)))

    def scan(self, base_path, recursive=True):
        """Scan a directory and sub directories"""
        for dir_name in os.listdir(base_path):

            # 1) skip anything that isn't a directory
            dir_path = base_path +"/"+ dir_name
            if not os.path.isdir(base_path + "/"+ dir_name):
                continue

            # 2) skip anything we don't have access to
            if not os.access(dir_path, os.R_OK):
                self.no_access.append(dir_path)
                continue

            # 3) try to match a run directory
            if RUN_DIR_REGEX.match(dir_name):
                if not OLD_RUN_DIR_REGEX.match(dir_name):
                    for file in os.listdir(dir_path):
                        if NGSQC_REGEX.match(file):
                            if file not in self.runs:
                                self.runs[file] = dir_path +"/"+ file
                            else:
                                self.runs[file] = dir_path +"/"+ file
                                logging.debug("Duplicate Run Found, skipping duplicate: "+ str(dir_name))
                                logging.debug(str(dir_path +"/"+ file) + " || " + str(self.runs[file]))
                                self.duplicates.append(dir_path)   
                            break
            elif recursive:
                self.scan(base_path +"/"+ dir_name)


def main():
    # set email_flag = True if want to enable email senting
    email_flag = False

    #scanned_file_path = pathlib.Path.cwd() / "scanned_info.json"
    scanned_file_path = pathlib.Path("/home/sbzhang/scripts/pythonscripts/NGSQC/fixed_scanned_info.json")
    #new_file_path = pathlib.Path.cwd() / "new_file.json"
    new_file_path = pathlib.Path("/home/sbzhang/scripts/pythonscripts/NGSQC/fixed_new_file.json")
    run_scanner = RunScanner()

    base_root_ls = generate_paths()
    for RUN_DIR_BASE_PATH in base_root_ls:
        logging.info("Scanning: " + RUN_DIR_BASE_PATH)
        run_scanner.scan(RUN_DIR_BASE_PATH)

    run_scanner.print_stats()


    past_scanned = {}

    if(os.path.exists(scanned_file_path)):
        logging.info("Checking Last time scan")
        past_scanned = read_json_to_dict(scanned_file_path)
        new_files = compare_diff(past_scanned, run_scanner.runs)
        write_dict_to_json(new_files, new_file_path)
        logging.info("Number of New NGSQC File: " + str(len(new_files)))
        logging.info("New NGSQC File Located In: " + str(new_file_path))

    else:
        logging.info("First Time Scan")
        write_dict_to_json(run_scanner.runs, new_file_path)

    write_dict_to_json(run_scanner.runs, scanned_file_path)

    if email_flag:
        email_address = 'sbzhang@predicine.com'
        content = '\nScanning complete'
        send_email(email_address,content)

if __name__ == "__main__":
    main()    
