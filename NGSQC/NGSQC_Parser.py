import pandas as pd
import json
import pathlib
import re
import logging
import mimetypes
import requests

logging.basicConfig(
    level=logging.DEBUG,
    format="%(levelname)s - %(asctime)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)



class NGSQC_Parser:
    # initialize the matching mapping and keyword control mapping
    def __init__(self):
        self.field_mapping = self.field_matching_map()
        self.keyword_mapping = self.keyword_matching_map()
        # add keyword control pattern
        self.PATTERN_V1 = re.compile("PREDICINEMRD_V1_+P[a-zA-Z0-9]+")
        self.PATTERN_V1B = re.compile("PREDICINEMRD_V1B_+P[a-zA-Z0-9]+")

    # create a dictionary map the fields
    def field_matching_map(self):
        # if new entity field needs, just add it to the mapping dictionary
        # concate the the field name and field type into string with format "Field_Name, Type"
        mapping = dict()
        mapping["sample_name"] = "SampleID, char"
        # update
        mapping["total_read_count_m"] = "Total reads (M), float" 
        # update
        mapping["mapped_read_count_m"] = "Mapped reads (M), float" 
        mapping["mapping_rate"] = "Mapping rate (%), float" 
        # update
        mapping["consensus_read_count_m"] = "Consensus reads (M), float" 
        mapping["duplication_rate"] = "Duplication rate (%), float"
        mapping["median_targ_uniq_frag_cov"] = "Target unique fragment coverage, int"  
        mapping["median_targ_uniq_read_cov"] = "Target read coverage, int" 
        mapping["mean_targ_read_cov"] = "Mean target read coverage, float" 
        mapping["prct_loci_gt_20_mean_cov"] = "Percentage of target > 20% Mean coverage, float" 
        mapping["prct_probe_cov_2_fold_a_consensus"] = "Within 2-fold (%) after dedup, float" 
        mapping["prct_probe_cov_2_fold_b_consensus"] = "Within 2-fold (%) before dedup, float" 
        mapping["read_on_targ_rate"] = "onTarget rate (%), float" 
        mapping["read_on_targ_200_flank_rate"] = "onTarget rate ext200 (%), float" 
        mapping["gc_ratio_low_middle"] = "gcRatio.low, float" 
        mapping["gc_ratio_high"] = "gcRatio.high, float" 
        mapping["mis_match_base_rate"] = "Mean error rate (%) after dedup, float" 
        mapping["bkgnd_50_a_dedup"] = "background.50% after dedup, float" 
        mapping["bkgnd_99_a_dedup"] = "background.99% after dedup, float" 
        mapping["frag_size_mode"] = "Fragment_Size_Mode, float" 
        mapping["soft_clip_ratio"] = "Softclip ratio, float" 
        mapping["bc_total_count"] = "BC_totalCount, int" 
        mapping["snp_bc"] = "SNP_BC, char" 
        mapping["cov_gt_4000"] = "Coverage > 4000 (%), float" 
        mapping["cov_gt_3000"] = "Coverage > 3000 (%), float" 
        mapping["cov_gt_2000"] = "Coverage > 2000 (%), float" 
        mapping["cov_gt_1000"] = "Coverage > 1000 (%), float" 
        mapping["qc_flag"] = "QC_Status, char" 
        mapping["action"] = "Action, char" 
        mapping["topoff_load_level"] = "Load, float" 
        mapping["snv_count"] = "SNV_Num, int" 
        mapping["indel_count"] = "Indel_Num, int" 
        mapping["tmp_variant_count"] = "pTMB, int" 
        mapping["norm_tmp_score"] = "pTMB_norm, float" 
        mapping["adj_tmp_variant_count"] = "pTMB.adj, float" 
        mapping["adj_norm_tmp_score"] = "pTMB_norm.adj, float" 
        mapping["msaf"] = "maxAF_TMB, float" 
        # update
        mapping["s_specimen_type"] = "Specimen_type, char"
        mapping["s_specimen_type_orig"] = "Specimen_type, char"  
        mapping["p_analysis_type"] = "Analysis_type, char" 
        mapping["d_gender"] = "Gender, char" 
        # update
        mapping["el_panel_name"] = "Panel, char"
        mapping["el_panel_name_orig"] = "Panel, char"  
        mapping["p_project_dir"] = "ProjectDir, char" 
        mapping["flowcell_lane"] = "lane, char" 
        mapping["work_order_info"] = "work_order, char" 
        mapping["adapter"] = "original_adapter, char" 
        mapping["ali_na_dna_amout_ng"] = "total_dna_input, float" 
        mapping["ali_na_cfdna_amount_ng"] = "total_cfdna_input, float"
        #update
        mapping["lb_yield_ug"] = "library_yield_ug, float" 
        mapping["ali_lb_amount_ug"] = "library_dna_input_ug, float" 
        mapping["el_molarity_nm"] = "enriched_library_fa_or_ba_nm, float" 
        mapping["el_conc_ng_ul"] = "qubit_ng_ul, float" 
        mapping["pipeline_version"] = "pversion, char"
        #update 
        mapping["s_study_type"] = "category, char"
        mapping["s_study_type_orig"] = "category, char"  
        mapping["d_patient_id"] = "PatientID, char" 
        mapping["s_external_id"] = "externalSampleID, char"
        return mapping

    # create a dictionary map the keyword filter matching
    # all key should be transfer to uppercase first
    def keyword_matching_map(self):
        mapping = dict()

        # Category

        # RUO
        mapping["RUO"] = "RUO"
        # CLIA
        mapping["CLIA"] = "CLIA" 
        mapping["CO"] = "CLIA"
        # RnD
        mapping["RND"] = "RnD"
        # Validation
        mapping["VALIDATION"] = "Validation"
        # IVD
        mapping["IVD"] = "IVD"

        # Specimen_type

        # Whole blood 
        mapping["Fresh WB"] = "Whole blood"
        mapping["Fresh_WB"] = "Whole blood"
        mapping["FRESHWB"] = "Whole blood"
        mapping["WHOLE_BLOOD"] = "Whole blood"
        mapping["WHOLEBLOOD"] = "Whole blood"
        # Plasma
        mapping["PLASMA"] = "Plasma"    
        mapping["FROZENPLASMA"] = "Plasma"
        mapping["FROZENPLASMA"] = "Plasma"
        mapping["CFDNA_ANALYSIS"] = "Plasma"
        # cfDNA   
        mapping["CFDNA"] = "cfDNA"
        # RNA
        mapping["RNA"] = "RNA"
        # Buffy Coat
        mapping["BUFFY_COAT"] = "Buffy Coat"
        # gDNA
        mapping["GDNA"] = "gDNA"
        # Serum
        mapping["SERUM"] = "Serum"
        # Urine
        mapping["URINE"] = "Urine"
        mapping["UCFDNA_ANALYSIS"] = "Urine"
        # Cell pellet
        mapping["CELL PELLET"] = "Cell pellet"
        mapping["CELL_PELLET"] = "Cell pellet"
        # FFPE 
        mapping["FFPE"] = "FFPE"
        mapping["TISSUE"] = "FFPE"
        mapping["FFPE SLIDES"] = "FFPE"
        mapping["FFPE_SLIDES"] = "FFPE"
        mapping["FFPEE BLOCK"] = "FFPE"
        # FFPE DNA
        mapping["FFPE DNA"] = "FFPE DNA"
        # Bone Marrow Aspirate
        mapping["BONE MARROW"] = "Bone Marrow Aspirate"
        mapping["BONE_MARROW"] = "Bone Marrow Aspirate"
        mapping["BONE_MARROW_ASPIRATE"] = "Bone Marrow Aspirate"
        # CSF
        mapping["CEREBRAL_SPINAL_FLUID"] = "CSF"
        mapping["CSF"] = "CSF"
        # Platelet
        mapping["PLATELET"] = "Platelet"
        # Nucleic Acid
        mapping["NUCLEIC_ACID"] = "Nucleic Acid"
        # Cryopreserved cells
        mapping["PBMC_BM"] = "Cryopreserved cells"
        mapping["CRYOPRESERVED CELLS"] = "Cryopreserved cells"
        mapping["PBMC"] = "Cryopreserved cells"
        mapping["CRYO PELLET"] = "Cryopreserved cells"
        mapping["CRYOPRESERVED PBMC"] = "Cryopreserved cells"
        # Cell line
        mapping["CELL LINE"] = "Cell line"
        mapping["CELL_LINE"] = "Cell line"
        mapping["CELLLINE"] = "Cell line"
        mapping["MIXED_CELLLINE"] = "Cell line"
        # Elution buffer
        mapping["WATER"] = "Elution buffer"
        mapping["EB"] = "Elution buffer"
        mapping["EB_BUFFER"] = "Elution buffer"
        mapping["EB_WATER"] = "Elution buffer"
        # Unclear
        mapping["COVIDCAPTURE"] = "Unclear"
        mapping["MIXED"] = "Unclear"
        mapping["UNKNOWN"] = "Unclear"
        mapping["UNKNOWN_SPECIMENTYPE"] = "Unclear"
        # Swab
        mapping["SWAB"] = "Swab"

        # Panel
        
        # PREDICINECARE_V2B
        mapping["PREDICINECARE_V2B"] = "PREDICINECARE_V2B"
        
        # PREDICINECARELITE_V1
        mapping["PREDICINECARELITE_V1"] = "PREDICINECARELITE_V1"
        
        # PREDICINEWES_V1
        mapping["PREDICINEWES_V1"] = "PREDICINEWES_V1"
        mapping["WES"] = "PREDICINEWES_V1"
        mapping["IDT_EXOME_V2__PREDICINEATLAS_V2__PREDICINEHRD_V1"] = "PREDICINEWES_V1"
        # PREDICINEATLAS_V1
        mapping["PREDICINEATLAS_V1"] = "PREDICINEATLAS_V1"
        mapping["PREDICINEATLAS"] = "PREDICINEATLAS_V1"
        # PREDICINELDT_V1
        mapping["PREDICINELDT_V1"] = "PREDICINELDT_V1"
        mapping["PREDICINELITE_V1__DDR_V2__DDR_PATCH"] = "PREDICINELDT_V1"
        mapping["PREDICINELDT"] = "PREDICINELDT_V1"
        # PREDICINERESEARCH
        mapping["PREDICINE_RESEARCH"] = "PREDICINERESEARCH"
        mapping["PREDICINERESEARCH"] = "PREDICINERESEARCH"
        # PREDICINEDDR_V2
        mapping["PREDICINEDDR_V2"] = "PREDICINEDDR_V2"
        mapping["DDR_V2"] = "PREDICINEDDR_V2"
        # PredicineLITE_V1        
        mapping["PREDICINELITE_V1"] = "PredicineLITE_V1"
        mapping["PREDICINELITE"] = "PredicineLITE_V1"
        # PREDICINEPCA_V1
        mapping["PREDICINEPCA_V1"] = "PREDICINEPCA_V1"
        mapping["PREDICINEPCA"] = "PREDICINEPCA_V1"        
        # PREDICINEPCA_V1
        mapping["RNA_FUSION_V1__RNA_HS_V1__RNA_FUS_PATCH"] = "PREDICINERNA_V1"
        mapping["RNA_FUSION_V1__RNA_HS_V1__RNA_FUSION_PATCH"] = "PREDICINERNA_V1"
        mapping["RNA_HS_V1__RNA_FUSION_V1__RNA_FUSION_PATCH__ALK"] = "PREDICINERNA_V1"
        # PREDICINEFUS_V1
        mapping["RNA_Fusion_V1__RNA_FUS_Patch"] = "PREDICINEFUS_V1"
        mapping["RNA_FUSION_V1__RNA_FUSION_PATCH"] = "PREDICINEFUS_V1"
        mapping["PREDICINEFUS"] = "PREDICINEFUS_V1"
        # PAN_V4
        mapping["PAN_V4"] = "PAN_V4"
        mapping["PAN_ALL3"] = "PAN_V4"
        # PAN_V4_CORE__DNA_FUSION
        mapping["PAN_V4_CORE__DNA_FUSION"] = "PAN_V4_CORE__DNA_FUSION"
        mapping["PANCERCANCER_CORE_FUSION"] = "PAN_V4_CORE__DNA_FUSION"
        # ACT_V1
        mapping["ACT_V1"] = "ACT_V1"
        mapping["PREDICINEACT_V1"] = "ACT_V1"

        return mapping

    # parse the csv file into json format
    def parse(self, filePath):
        df = pd.read_csv(filePath,dtype=str)
        logging.info("Read"+ ": " + str(filePath))
        # create a list store all rows
        result_list = []     
        try:
            # store one row in dict format and iterate through all dataframe
            for index in range(df.shape[0]):
                try:
                    data_row = dict()
                    # add run_id to the json data
                    data_row["run_id"] = str(filePath).split("/")[-2]
                    # transfer Field Name to Database Entity Field
                    for key in self.field_mapping:
                        value = self.field_mapping[key].split(", ")
                        field_name = value[0]
                        entity_type = value[1]
                        if field_name in df.columns:
                            acutal_value = str(df.loc[index,field_name])
                            if key == "s_study_type" or key == "el_panel_name" or key == "s_specimen_type" :
                                # keyword constrain
                                if acutal_value.upper() in self.keyword_mapping:
                                    acutal_value = self.keyword_mapping[acutal_value.upper()]
                                # Check if the acutal_value match the regex
                                if self.PATTERN_V1.match(acutal_value):
                                    acutal_value = "PREDICINEMRD_V1_personalized"
                                if self.PATTERN_V1B.match(acutal_value):
                                    acutal_value = "PREDICINEMRD_V1B_personalized"



                            if acutal_value != "nan":
                                if entity_type == "int":
                                    data_row[key] = int(acutal_value)
                                elif entity_type == "float":
                                    if acutal_value == "-Inf" or acutal_value == " -Inf" or acutal_value == "  -Inf":  
                                        data_row[key] = None
                                    else:
                                        data_row[key] = float(acutal_value)
                                else:
                                    data_row[key]= acutal_value
                            elif acutal_value == "null":
                                data_row[key] = None   
                            else:
                                data_row[key] = None
                        else:
                            data_row[key] = None
                except:
                    logging.error("Errors in line: " +  str(index + 2))
                    raise ValueError("Errors in line:" + str(index + 2))
                
                # append the data_row to result list
                result_list.append(data_row)
            
            return(result_list)
        except:
            logging.error("Cannot read file"+ ": " + str(filePath))
            raise ValueError("Cannot read file"+ ": " + str(filePath))




def main():
    parser = NGSQC_Parser()
    file_path = pathlib.Path("/prednet/data03/DevHome/kiwi_file_maintenance/files_after_fix/220204_A00934_0157_BH5LGVDSX3/220204_A00934_0157_BH5LGVDSX3_combined_NGSQC_Summary_all.csv")
    result = parser.parse(file_path)
    print(result)

if __name__ == "__main__":
    main()
