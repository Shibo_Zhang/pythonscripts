1.  Initialize the python environment on the cluster with Anaconda or miniconda
Simple Tutorial
https://problemsolvingwithpython.com/01-Orientation/01.05-Installing-Anaconda-on-Linux/#:~:text=Installing%20Anaconda%20on%20Linux.%201%201.%20Visit%20the,Run%20the%20bash%20script%20to%20install%20Anaconda3.%20
2.  Install all dependencies by command

    _pip install -r requirements.txt_

NGSQC Scripts and Sample Sheet Scripts are under two different directories

3. NGSQC Files Scanning Parsing and Uploading
- [Scanning ] _python3 NGSQC_Scanning.py_
- [Parsing ] _python3 NGSQC_Parsing.py_
- [Uploading ] _python3 NGSQC_Upload.py_
4. Sample Sheet Files Scanning Parsing and Uploading 
- [Scanning ]  _python3 SampleSheetScanning.py_
- [Parsing ] _python3 SampleSheetParsing.py_
- [Uploading ] _python3 SampleSheetUpload.py_

For sacnning scripts, the result will store in scanned_info.json and newfile.json two files.
scanned_info.json stores all files and file paths in each run.
newfile.json stores the new files compared to the last Scanning.

5. Cron job Kiwi Services (Once a day)

Currently combined_NGSQC.py and combined_samplesheet.py contains the processes from scanning to uploading
Running these two files will report the errors to the LIMS Jira Board
- _python3 combined_NGSQC.py_
- _python3 combined_samplesheet.py_

6. Upload One file Scripts
- [NGSQC] _python3 upload_one.py File_Name File_Path_
- [SampleSheet] _python3 upload_one.py RUN_ID File_Path_
